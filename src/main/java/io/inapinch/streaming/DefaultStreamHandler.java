package io.inapinch.streaming;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Optional;
import java.util.Queue;
import java.util.concurrent.*;
import java.util.function.Consumer;
import java.util.function.Supplier;

final class DefaultStreamHandler<T> implements StreamHandler<T> {
    private final BlockingQueue<T> tempStorage;
    private final Supplier<Boolean> hasNext;
    private final Executor executor;

    DefaultStreamHandler(Supplier<Boolean> hasNext, Executor executor) {
        this(new LinkedBlockingDeque<>(), hasNext, executor);
    }
    DefaultStreamHandler(BlockingQueue<T> tempStorage, Supplier<Boolean> hasNext, Executor executor) {
        this.tempStorage = tempStorage;
        this.hasNext = hasNext;
        this.executor = executor;
    }

    @Override
    public void accept(T t) {
        try {
            tempStorage.put(t);
        } catch (InterruptedException e) {
            throw new RuntimeException("Failed to insert element", e);
        }
    }

    @Override
    public Iterator<T> iterator() {
        return new DefaultIterator<>(tempStorage, hasNext);
    }

    @Override
    public Iterable<T> run(Supplier<T> adapter) {
        executor.execute(() -> {
            while(hasNext.get()) {
                var value = adapter.get();
                if(value != null)
                    accept(value);
            }
        });

        return this;
    }
}
