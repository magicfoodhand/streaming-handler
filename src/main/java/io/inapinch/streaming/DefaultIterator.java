package io.inapinch.streaming;

import java.util.Iterator;
import java.util.concurrent.BlockingQueue;
import java.util.function.Supplier;

final class DefaultIterator<T> implements Iterator<T> {
    private final BlockingQueue<T> tempStorage;
    private final Supplier<Boolean> hasNext;

    DefaultIterator(BlockingQueue<T> tempStorage, Supplier<Boolean> hasNext) {
        this.tempStorage = tempStorage;
        this.hasNext = hasNext;
    }

    @Override
    public boolean hasNext() {
        return !tempStorage.isEmpty() || hasNext.get();
    }

    @Override
    public T next() {
        try {
            return tempStorage.take();
        } catch (InterruptedException e) {
            throw new RuntimeException("Failed to remove element", e);
        }
    }
}
