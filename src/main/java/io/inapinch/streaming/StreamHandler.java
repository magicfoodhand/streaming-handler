package io.inapinch.streaming;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.function.Consumer;
import java.util.function.Supplier;

public interface StreamHandler<T> extends Consumer<T>, Iterable<T> {
    /**
     *
     * @param adapter A supplier that returns a value or `null` if it should be ignored in the final result set
     * @return an Iterable that will eventually contain all the results
     */
    Iterable<T> run(Supplier<T> adapter);

    static <T> StreamHandler<T> create(Supplier<Boolean> hasNext) {
        return create(hasNext, Executors.newSingleThreadExecutor());
    }

    static <T> StreamHandler<T> create(Supplier<Boolean> hasNext, Executor executor) {
        return new DefaultStreamHandler<>(hasNext, executor);
    }
}