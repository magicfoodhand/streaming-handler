package io.inapinch.streaming;

import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.junit.jupiter.api.Assertions.*;

public class StreamHandlerTest {
    @Test
    public void testStreaming() {
        var calls = new AtomicInteger(0);
        Iterable<Integer> iterator = StreamHandler.<Integer>create(() -> calls.get() < 3)
                .run(calls::incrementAndGet);

        var results = StreamSupport.stream(iterator.spliterator(), false)
                .collect(Collectors.toList());

        var expected = List.of(1, 2, 3);

        assertEquals(expected, results);
    }

    @Test
    public void testIgnoresNullValues() {
        var calls = new AtomicInteger(0);
        var results = StreamHandler.create(() -> calls.get() < 2).run(() ->
                calls.getAndIncrement() % 2 == 0 ? null : calls.get()
        );

        var iterator = results.iterator();
        assertEquals(2, iterator.next());
        assertFalse(iterator.hasNext());
    }

    @Test
    public void exceptionsAreThrown_next() {
        var iterator = new DefaultIterator<>(new BadQueue<>(), () -> false);
        assertTrue(iterator.hasNext());
        assertThrows(RuntimeException.class, iterator::next);
    }

    @Test
    public void exceptionsAreThrown_accept() {
        var handler = new DefaultStreamHandler<String>(new BadQueue<>(), () -> false, Executors.newSingleThreadExecutor());
        assertThrows(RuntimeException.class, () -> handler.accept("test"));
    }

    static final class BadQueue<T> implements BlockingQueue<T> {

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public void put(T t) throws InterruptedException {
            throw new InterruptedException("Oh the humanity");
        }

        @Override
        public T take() throws InterruptedException {
            throw new InterruptedException("Oh the humanity");
        }


        @Override
        public boolean add(T t) {
            return false;
        }

        @Override
        public boolean offer(T t) {
            return false;
        }

        @Override
        public T remove() {
            return null;
        }

        @Override
        public T poll() {
            return null;
        }

        @Override
        public T element() {
            return null;
        }

        @Override
        public T peek() {
            return null;
        }

        @Override
        public boolean offer(T t, long l, TimeUnit timeUnit) throws InterruptedException {
            return false;
        }

        @Override
        public T poll(long l, TimeUnit timeUnit) throws InterruptedException {
            return null;
        }

        @Override
        public int remainingCapacity() {
            return 0;
        }

        @Override
        public boolean remove(Object o) {
            return false;
        }

        @Override
        public boolean containsAll(Collection<?> collection) {
            return false;
        }

        @Override
        public boolean addAll(Collection<? extends T> collection) {
            return false;
        }

        @Override
        public boolean removeAll(Collection<?> collection) {
            return false;
        }

        @Override
        public boolean retainAll(Collection<?> collection) {
            return false;
        }

        @Override
        public void clear() {

        }

        @Override
        public int size() {
            return 0;
        }

        @Override
        public boolean contains(Object o) {
            return false;
        }

        @Override
        public Iterator<T> iterator() {
            return null;
        }

        @Override
        public Object[] toArray() {
            return new Object[0];
        }

        @Override
        public <T1> T1[] toArray(T1[] t1s) {
            return null;
        }

        @Override
        public int drainTo(Collection<? super T> collection) {
            return 0;
        }

        @Override
        public int drainTo(Collection<? super T> collection, int i) {
            return 0;
        }
    }
}
