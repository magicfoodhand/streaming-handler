# Streaming Handler
[![pipeline status](https://gitlab.com/inapinch/streaming-handler/badges/master/pipeline.svg)](https://gitlab.com/inapinch/streaming-handler/-/commits/master)
[![coverage report](https://gitlab.com/inapinch/streaming-handler/badges/master/coverage.svg)](https://gitlab.com/inapinch/streaming-handler/-/commits/master)

## Usage

### Add Maven Dependency
```xml
<dependency>
    <groupId>io.inapinch</groupId>
    <artifactId>streaming-handler</artifactId>
    <version>1.0</version>
</dependency>
```

### Running
```java
import io.inapinch.streaming.StreamHandler;

ResultSet rs;

// Both ResultSet method calls require proper error handling, excluded here for brevity.
var handler = StreamHandler.<Integer>create(rs::next);
var iterable = handler.run(() -> {
    var meaningOfLife = rs.getInt("meaningOfLife");
    
    if(meaningOfLife != 42)
        return null;
    
    return meaningOfLife;
});

var results = StreamSupport.stream(iterable.spliterator(), false).collect(Collectors.toList());
assert(results.get(0) == 42);
```